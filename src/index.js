import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import { Dashboard } from './components';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Dashboard />, document.getElementById('root'));
registerServiceWorker();
