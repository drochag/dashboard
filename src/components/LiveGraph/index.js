import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { makeWidthFlexible, XYPlot, XAxis, YAxis, HorizontalGridLines, LineMarkSeries, LabelSeries } from 'react-vis';

import { parseTime } from '../../utils';

const FlexibleXYPlot = makeWidthFlexible(XYPlot);

function xAxisValues (t, i) {
  return (<tspan>{parseTime(t)}</tspan>);
}

class LiveGraph extends Component {
  render() {
    const { eth, btc, className } = this.props;

    if (!eth.length || !btc.length) {
      return <div className="LiveGraph" />
    }

    return (
      <div className={`LiveGraph ${className}`}>
        <h1>Live Graph</h1>
        <FlexibleXYPlot
          className="LiveGraph-graph"
          height={300}>
          <HorizontalGridLines />
          <XAxis tickTotal={eth.length} tickFormat={xAxisValues} />
          <YAxis />
          <LineMarkSeries style={{strokeLinejoin: "round"}} data={eth} stroke="#18BC9C" />
          <LabelSeries data={eth} />
          <LineMarkSeries style={{strokeLinejoin: "round"}} data={btc} stroke="#3498DB" />
          <LabelSeries data={btc} />
        </FlexibleXYPlot>
      </div>
    );
  }
}

LiveGraph.propTypes = {
  eth: PropTypes.arrayOf(PropTypes.shape({})),
  btc: PropTypes.arrayOf(PropTypes.shape({})),
  className: PropTypes.string
};

LiveGraph.defaultProps = {
  eth: [],
  btc: [],
  className: ''
};

export default LiveGraph;
