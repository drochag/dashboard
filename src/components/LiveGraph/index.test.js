import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { LineMarkSeries } from 'react-vis';

import { LiveGraph } from '../';

Enzyme.configure({ adapter: new Adapter() });

const mockData = {
  eth: [
    {x: 1, y: 10},
    {x: 2, y: 5},
    {x: 3, y: 15}
  ],
  btc: [
    {x: 1, y: 10},
    {x: 2, y: 5},
    {x: 3, y: 15}
  ]
};

describe('LiveGraph', () => {
  let mountedLiveGraph;
  const liveGraph = (props) => {
    if (!mountedLiveGraph) {
      mountedLiveGraph = mount(
        <LiveGraph {...props} />
      );
    }
    return mountedLiveGraph;
  };

  beforeEach(() => {
    mountedLiveGraph = undefined;
  });

  it('renders without crashing', () => {
    expect(liveGraph().find('.LiveGraph').length).toBe(1);
  });

  it('shouldn\'t render any `LineMarkSeries` if no data passed', () => {
    expect(liveGraph().find(LineMarkSeries).length).toBe(0);
  });

  it('renders 2 `LineMarkSeries` components', () => {
    expect(liveGraph(mockData).find(LineMarkSeries).length).toBe(2);
  });

  it('should add additional className', () => {
    expect(liveGraph({ ...mockData, className: 'AdditionalClassName' }).find('.LiveGraph').hasClass('AdditionalClassName')).toBeTruthy();
  });
});
