import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { format } from 'currency-formatter';

class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      conversion: '$0'
    };

    this.updateQuantity = this.updateQuantity.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.updateQuantity(nextProps);
  }

  updateQuantity (props) {
    if (!this.currency) {
      return;
    }

    const refProps = props || this.props;
    const currency = this.currency.value;
    const cryptoCurrency = this.cryptoCurrency.value;
    const quantity = this.quantity.value;
    this.setState({ conversion: format(refProps[cryptoCurrency.toLowerCase()][currency] * quantity, { code: 'USD' }) });
  }

  render() {
    const commonProps = {
      onChange: evt => this.updateQuantity(),
      className: 'form-control',
      type: 'number'
    };

    return (
      <div className={`Calculator ${this.props.className}`}>
        <div className="Calculator-container">
          <h1>Calculator</h1>
          <div className="Calculator-form">
            <div className="col-6">
              <div className="form-group">
                <input id="quantity" {...commonProps} placeholder="Enter quantity" ref={(input) => { this.quantity = input; }} />
              </div>
              <div className="form-group">
                <div id="conversion" {...commonProps} disabled>{this.state.conversion}</div>
              </div>
            </div>
            <div className="col-6">
              <div className="form-group">
                <select id="currency" onChange={commonProps.onChange} className="form-control" ref={(input) => { this.currency = input; }}>
                  <option>USD</option>
                  <option>EUR</option>
                </select>
              </div>
              <div className="form-group">
                <select id="cryptoCurrency" onChange={commonProps.onChange} className="form-control" ref={(input) => { this.cryptoCurrency = input; }}>
                  <option>BTC</option>
                  <option>ETH</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Calculator.propTypes = {
  btc: PropTypes.shape({}),
  eth: PropTypes.shape({}),
  className: PropTypes.string
};

Calculator.defaultProps = {
  eth: {},
  btc: {},
  className: ''
};

export default Calculator;
