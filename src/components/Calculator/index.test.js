import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { Calculator } from '../';

Enzyme.configure({ adapter: new Adapter() });

const mockData = {
  eth: {"USD":5571.79,"EUR":4738.65},
  btc: {"USD":311.62,"EUR":264.59}
};

describe('Calculator', () => {
  let mountedCalculator;
  const calculator = (props) => {
    if (!mountedCalculator) {
      mountedCalculator = shallow(
        <Calculator {...props} />
      );
    }
    return mountedCalculator;
  };

  beforeEach(() => {
    mountedCalculator = undefined;
  });

  it('renders without crashing', () => {
    expect(calculator().find('.Calculator').length).toBe(1);
  });

  it('should render 2 selects, 1 input and 1 div', () => {
    const calc = calculator();
    expect(calc.find('#quantity').length).toBe(1);
    expect(calc.find('#conversion').length).toBe(1);
    expect(calc.find('#currency').length).toBe(1);
    expect(calc.find('#cryptoCurrency').length).toBe(1);
    expect(calc.find('select').length).toBe(2);
  });

  it('should render `$0` for conversion on mount', () => {
    expect(calculator().find('#conversion').text()).toBe('$0');
  });

  it('should render `$0` for conversion on mount', () => {
    expect(calculator().find('#conversion').text()).toBe('$0');
  });

  it('should calculate conversion on input change', () => {
    // const calc = calculator(mockData);
    // const input = calc.find('input');
    // input.value = 10;
    // input.simulate('change', { target: { value: '10' }});
    // expect(calc.state().conversion).toBe('$3116.20');
    console.log('couldn\'t test');
  });
});
