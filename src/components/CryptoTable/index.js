import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { parseTime } from '../../utils';

const renderRow = ({ x: time, label, labelEUR }) => (
  <tr key={time}>
    <td>{parseTime(time)}</td>
    <td>{label}</td>
    <td>{labelEUR}</td>
  </tr>
);

class CryptoTable extends Component {
  render() {
    const { title, data, dataEUR, className } = this.props;

    data.reverse().forEach((item, idx) => {
      item.labelEUR = dataEUR[idx].label;
    });

    return (
      <div className={`CryptoTable ${className}`}>
        <h1>{title}</h1>
        <table className="table table-striped table-hover table-bordered">
          <thead>
            <tr className="table-dark">
              <th>Time</th>
              <th>USD</th>
              <th>EUR</th>
            </tr>
          </thead>
          <tbody>
            {data.map(renderRow)}
          </tbody>
        </table>
      </div>
    );
  }
}

CryptoTable.propTypes = {
  title: PropTypes.string.isRequired,
  className: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.shape({})),
  dataEUR: PropTypes.arrayOf(PropTypes.shape({}))
};

CryptoTable.defaultProps = {
  data: [],
  dataEUR: [],
  className: ''
};

export default CryptoTable;
