import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { format } from 'currency-formatter';

class PriceTitles extends Component {
  render() {
    const { eth, btc, className, currency } = this.props;

    if (!Object.keys(eth).length || !Object.keys(btc).length) {
      return <div className="PriceTitles" />
    }

    return (
      <div className={`PriceTitles ${className}`}>
        <h3 className="PriceTitles-price">
          1 BTC = {format(btc[currency], { code: currency })} {currency}
        </h3>
        <h3 className="PriceTitles-price">
          1 ETH = {format(eth[currency], { code: currency })} {currency}
        </h3>
      </div>
    );
  }
}

PriceTitles.propTypes = {
  btc: PropTypes.shape({}),
  eth: PropTypes.shape({}),
  currency: PropTypes.string,
  className: PropTypes.string
};

PriceTitles.defaultProps = {
  eth: {},
  btc: {},
  currency: 'USD',
  className: ''
};

export default PriceTitles;
