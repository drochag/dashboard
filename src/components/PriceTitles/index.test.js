import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { LineMarkSeries } from 'react-vis';

import { PriceTitles } from '../';

Enzyme.configure({ adapter: new Adapter() });

const mockData = {
  btc: {"USD":5571.79,"EUR":4738.65},
  eth: {"USD":311.62,"EUR":264.59}
};

describe('PriceTitles', () => {
  let mountedPriceTitles;
  const priceTitles = (props) => {
    if (!mountedPriceTitles) {
      mountedPriceTitles = mount(
        <PriceTitles {...props} />
      );
    }
    return mountedPriceTitles;
  };

  beforeEach(() => {
    mountedPriceTitles = undefined;
  });

  it('renders without crashing', () => {
    expect(priceTitles().find('.PriceTitles').length).toBe(1);
  });

  it('shouldn\'t render any `.PriceTitles-price` if no data passed', () => {
    expect(priceTitles().find('.PriceTitles-price').length).toBe(0);
  });

  it('renders 2 `.PriceTitles-price`', () => {
    expect(priceTitles(mockData).find('.PriceTitles-price').length).toBe(2);
  });

  it('should add additional className', () => {
    expect(priceTitles({ ...mockData, className: 'AdditionalClassName' }).find('.PriceTitles').hasClass('AdditionalClassName')).toBeTruthy();
  });
});
