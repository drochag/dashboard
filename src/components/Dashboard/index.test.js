import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import moxios from 'moxios';

import { setupMock, request } from '../../utils';
import { Dashboard, LiveGraph, Calculator, CryptoTable } from '../';

Enzyme.configure({ adapter: new Adapter() });

describe('Dashboard', () => {
  let mountedDashboard;
  const dashboard = () => {
    if (!mountedDashboard) {
      mountedDashboard = mount(
        <Dashboard />
      );
    }
    return mountedDashboard;
  };

  beforeEach(() => {
    mountedDashboard = undefined;
    moxios.install(request);
    setupMock(moxios);
  });

  afterEach(function () {
    moxios.uninstall()
  })

  it('renders without crashing', () => {
    const divs = dashboard().find('div');
    expect(divs.length).toBeGreaterThan(0);
  });

  it('renders child components', done => {
    const component = dashboard();
    moxios.wait(() => {
      component.update();
      expect(component.find(Calculator).length).toBe(1);
      expect(component.find(LiveGraph).length).toBe(1);
      expect(component.find(CryptoTable).length).toBe(2);
      done();
    });
  });
});
