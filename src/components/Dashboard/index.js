import React, { Component } from 'react';

import { LiveGraph, CryptoTable, Calculator, PriceTitles } from '../';
import logo from './logo.svg';
import * as request from '../../utils';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.doRequests = this.doRequests.bind(this);
    this.startTicker = this.startTicker.bind(this);

    this.state = {
      data: []
    };
  }

  componentWillMount() {
    this.doRequests();
    this.startTicker();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  startTicker() {
    this.interval = setInterval(() => this.doRequests(), 60 * 1000);
  }

  doRequests() {
    request.all([
      request.getHisToMinute('ETH'),
      request.getHisToMinute('ETH', 'EUR'),
      request.getHisToMinute('BTC'),
      request.getHisToMinute('BTC', 'EUR'),
      request.getPriceMulti()
    ])
      .then(([
        { from, to, data: eth }, // ETH
        { data: ethEUR }, // ETH EUR
        { data: btc }, // BTC
        { data: btcEUR }, // BTC EUR
        prices
      ]) => {
        this.setState({
          from, to, eth, btc, prices, ethEUR, btcEUR
        });
      });
  }

  render() {
    const { prices, eth, btc, btcEUR, ethEUR } = this.state;
    const header = (
      <header className="Dashboard-header">
        <img src={logo} className="Dashboard-logo" alt="logo" />
        <h1 className="Dashboard-title">CryptoCurrency Dashboard</h1>
      </header>
    );

    if (!prices) {
      return (
        <div className="Dashboard">
          {header}
          <div className="Dashboard-row">
            <h1 className="Dashboard-title">Loading</h1>
          </div>
        </div>
      )
    }

    return (
      <div className="Dashboard">
        {header}
        <div className="Dashboard-row">
          <LiveGraph eth={eth} btc={btc} className="Dashboard-liveGraph" />
          <Calculator className="Dashboard-calculator" eth={prices.ETH} btc={prices.BTC} />
        </div>
        <div className="Dashboard-pricesContainer">
          <PriceTitles className="Dashboard-priceTitles" eth={prices.ETH} btc={prices.BTC} />
          <PriceTitles className="Dashboard-priceTitles" eth={prices.ETH} btc={prices.BTC} currency='EUR' />
        </div>
        <div className="Dashboard-row">
          <CryptoTable data={btc} dataEUR={btcEUR} title="Bitcoin - BTC" className="Dashboard-cryptoTable" />
          <CryptoTable data={eth} dataEUR={ethEUR} title="Etherium - ETH" className="Dashboard-cryptoTable" />
        </div>
      </div>
    );
  }
}

export default Dashboard;
