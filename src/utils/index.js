import axios, { all } from 'axios';
import { format } from 'currency-formatter';

const request = axios.create({
  baseURL: 'https://min-api.cryptocompare.com/data/'
});

const getHisToMinute = (target, currency = 'USD') => request.get(`histominute?fsym=${target}&tsym=${currency}&limit=4&aggregate=1`)
  .then(res => res.data)
  .then(({ Data: data, TimeTo: to, TimeFrom: from }) => ({ data, to, from }))
  .then(
    ({ data, to, from }) =>
    ({ from, to, data: data.map( ({ time: x, close: y }) => ({ x, y, label: format(y, { code: 'USD' }) }) ) })
  );

const getPriceMulti = () => request.get('pricemulti?fsyms=BTC,ETH&tsyms=USD,EUR').then(res => res.data);

const setupMock = moxios => {
  moxios.stubRequest(/https:\/\/min-api.cryptocompare.com\/data\/histominute.*/, {
    status: 200,
    response: {"Response":"Success","Type":100,"Aggregated":false,"Data":[{"time":1508280780,"close":311.89,"high":311.89,"low":311.89,"open":311.89,"volumefrom":364.58,"volumeto":113519.11},{"time":1508280840,"close":311.86,"high":311.93,"low":311.56,"open":311.86,"volumefrom":365.92,"volumeto":114012.25},{"time":1508280900,"close":311.79,"high":311.89,"low":311.79,"open":311.87,"volumefrom":267.18,"volumeto":83391.2},{"time":1508280960,"close":311.78,"high":311.79,"low":311.73,"open":311.79,"volumefrom":477.01,"volumeto":148702.7},{"time":1508281020,"close":311.62,"high":311.79,"low":311.58,"open":311.78,"volumefrom":240.57,"volumeto":75009.06}],"TimeTo":1508281020,"TimeFrom":1508280780,"FirstValueInArray":true,"ConversionType":{"type":"direct","conversionSymbol":""}}
  });

  moxios.stubRequest(/https:\/\/min-api.cryptocompare.com\/data\/pricemulti.*/, {
    status: 200,
    response: {"BTC":{"USD":5571.79,"EUR":4738.65},"ETH":{"USD":311.62,"EUR":264.59}}
  });
};

const parseTime = (t) => {
  const date = new Date(parseInt(t, 10) * 1000);
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let seconds = date.getSeconds();

  if (hours < 10) {
    hours = '0' + hours;
  }

  if (minutes < 10) {
    minutes = '0' + minutes;
  }

  if (seconds < 10) {
    seconds = '0' + seconds;
  }

  return `${hours}:${minutes}:${seconds}`;
}

export { request, parseTime, setupMock, all, getHisToMinute, getPriceMulti };